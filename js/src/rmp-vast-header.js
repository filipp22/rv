/**
 * @license Copyright (c) 2017-2020 Radiant Media Player | https://www.radiantmediaplayer.com
 * rmp-vast 2.5.0
 * GitHub: https://github.com/azazar/rmp-vast
 * MIT License: https://github.com/azazar/rmp-vast/blob/master/LICENSE
 */
