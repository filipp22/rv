(function () {




    // Settings ///

    const containerId = 'rmpPlayer';

    const adTags = [
        'vast.xml',
        'https://www.radiantmediaplayer.com/vast/tags/inline-linear-skippable-per-cent.xml',
        'https://www.3movs.com/sd3/s/s/suo.php?rc=1&referrer_url=',
        'https://www.3movs.com/sd3/s/s/suo.php?rc=1&index=1&referrer_url=',
        'https://www.3movs.com/sd3/s/s/suo.php?rc=1&index=2&referrer_url=',
    ];

    const params = {
        displayClickLink: true,
        styleForClickLink: 'color: #0f0',
        clickLinkDelay: 5,
        skipOverride: 1,
        enableVpaid: true,
        preloadAds: true,
        ajaxTimeout: 8000,
    };

    const cssUrl = '../css/rmp-vast.css';
    const jsUrl = '../js/dist/rmp-vast.min.js';

    let xml;

    //////////////

    window.runAdmanPreroll = function () {
        window.setTimeout(() => {
            let container = document.getElementById(containerId);
            let type = 'native';
            if (container.childElementCount > 2)
                type = 'player'
            switch (type) {
                case "native":
                    window.rmpVast.play();
                    break;

                case "player":
                    for (let node of container.childNodes) {
                        if (node.nodeName != "A") {
                            node.click();
                            if (node.childElementCount)
                                for (let child of node.childNodes)
                                    child.click();

                        }
                    }
                    break;
            }


        }, 100)

    }

    function preloadAds(url) {
        let urlVast = url.length ? url[0] : url
        if (urlVast) {
            admanAjax(
                urlVast,
                params.ajaxTimeout,
                true,
                true
            ).then(data => {
                try {
                    const parser = new DOMParser();
                    xml = parser.parseFromString(data, 'text/xml');
                    if (!xml) {
                        url.splice(0, 1)
                        preloadAds(url)
                    } else {
                        let blob = new Blob([data], {type: 'text/xml'});
                        xml = URL.createObjectURL(blob);
                    }
                } catch (e) {
                    url.splice(0, 1)
                    preloadAds(url)
                    return;
                }

            }).catch(e => {
                url.splice(0, 1)
                preloadAds(url)
            });
        }
    }

    onNodeAdopted = (nodeName, callback) => {
        new MutationObserver((mutations, observer) => {
            mutations.forEach(function (mutation) {
                if (mutation.addedNodes.length) {
                    if (mutation.addedNodes[0].nodeName == nodeName) {
                        observer.disconnect();
                        callback(mutation.addedNodes[0]);
                    }
                }
            });
        }).observe(document.getElementById(containerId), {childList: true, subtree: true});
    }

    startVast = () => {
        window.rmpVast = new RmpVast(containerId, params);
        let nodes = document.querySelector('.rmp-content'),
            docPlayer = document.getElementById(containerId),
            urlAds = params.preloadAds ? xml : adTags;

        let isPlay = true
        window.rmpVast.contentPlayer.addEventListener('play', function (event) {
            if (isPlay) {
                window.rmpVast.loadAds(urlAds || adTags);
                window.rmpVast.play();
                isPlay = false
                event.preventDefault();
            }

        });

        docPlayer.addEventListener('adstarted', function () {
            if (nodes.childNodes)
                for (let node of nodes.childNodes) {
                    if (node.nodeName != "VIDEO"
                        && node.className != "rmp-ad-container"
                        && node.className != "vast-panel-control-pause"
                        && node.className != "vast-panel-control-play") {
                        node.style.visibility = 'hidden'
                        node.setAttribute('vast-hide', true)
                    }
                }
            document.getElementById('vast-play-pause').style.display = 'block'
        });

        docPlayer.addEventListener('adpaused', function () {
            document.getElementById('vast-play-pause').className = "vast-panel-control-play"
        });
        const eventListVisible = ['adclosed', 'aderror', 'addestroyed', 'adinitialplayrequestfailed']
        for (let i = 0; i < eventListVisible.length; i++) {
            docPlayer.addEventListener(eventListVisible[i], function () {
                if (nodes.childNodes)
                    for (let node of nodes.childNodes) {
                        if (node.hasAttribute('vast-hide'))
                            node.style.visibility = 'visible'
                    }
                document.getElementById('vast-play-pause').style.display = 'none'
            });
        }
    }

    const addVastControl = (video, callback) => {
        let playControl = document.createElement("div");
        playControl.id = 'vast-play-pause'
        playControl.appendChild(document.createElement("div"));
        playControl.classList.add('vast-panel-control-pause')
        video.parentNode.appendChild(playControl)
        playControl.addEventListener("click", function () {
            if (window.rmpVast.getAdPaused()) {
                window.rmpVast.play();
                this.className = "vast-panel-control-pause";
            } else {
                window.rmpVast.pause();
                this.className = "vast-panel-control-play";
            }
        });
        callback(video)
    }
    const wrapperVideoPlayer = (video, callback) => {
        let type = 'native';
        let selfWrapper = document.createElement('div');
        if (video.parentNode.childElementCount > 2)
            type = 'player'
        switch (type) {
            case "native":
                video.classList.add('rmp-video')
                selfWrapper.setAttribute('class', 'rmp-content');
                selfWrapper.innerHTML = video.outerHTML;
                video.parentNode.replaceChild(selfWrapper, video)
                break;

            case "player":
                video.removeAttribute('preload')
                video.classList.add('rmp-video')
                video.parentNode.classList.add('rmp-content')
                break;
        }
        callback(video)
    }

    const attachVast = function () {
        let container = document.getElementById(containerId);
        if (container.clientWidth === 0 || container.clientHeight === 0) {
            window.setTimeout(attachVast, 100);
            return;
        }
        let video = container.querySelector('video');
        if (!video) {
            onNodeAdopted("VIDEO", function (video) {
                addVastControl(video, (result) => {
                    wrapperVideoPlayer(video, (video) => {
                        startVast()
                    })
                })
            })
        } else {
            addVastControl(video, (result) => {
                wrapperVideoPlayer(video, (video) => {
                    startVast()
                })
            })
        }
    };

    function admanAjax(url, timeout, returnData, withCredentials) {
        return new Promise((resolve, reject) => {
            if (window.XMLHttpRequest) {
                const xhr = new XMLHttpRequest();
                xhr.open('GET', url, true);
                xhr.timeout = timeout;

                if (withCredentials) {
                    xhr.withCredentials = true;
                }
                xhr.onloadend = function () {
                    if (xhr.status && xhr.status >= 200 && xhr.status < 300) {
                        if (typeof xhr.responseText === 'string' && xhr.responseText !== '') {
                            if (returnData) {
                                resolve(xhr.responseText);
                            } else {
                                resolve();
                            }
                        } else {
                            reject();
                        }
                    } else {
                        reject();
                    }
                };
                xhr.ontimeout = function () {
                    reject();
                };

                xhr.send(null);
            } else {
                reject();
            }
        });
    };

    const cssId = 'rmp_vast_css_link';
    if (params.preloadAds)
        preloadAds(adTags)//.then((data) => {}).catch(e => {});

    if (!document.getElementById(cssId)) {
        var link = document.createElement('link');
        link.setAttribute('id', cssId);
        link.setAttribute('rel', 'stylesheet');
        link.setAttribute('href', cssUrl);
        document.head.appendChild(link);
    }

    const jsId = 'rmp_vast_js';
    if (!document.getElementById(jsId)) {
        var script = document.createElement('script');
        script.setAttribute('id', jsId);
        script.setAttribute('type', 'text/javascript');
        document.head.appendChild(script);
        script.addEventListener('load', attachVast);
        script.setAttribute('src', jsUrl);
    } else {
        attachVast();
    }


})();


